Django==2.0.8
djangorestframework
markdown
django-filter
psycopg2
pyodbc
salic-ml==0.0.4
django-cors-headers
